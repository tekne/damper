use rand::Rng;

/// An action a `damper` robot can take
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Action {
    /// Enter long / exit short
    Long,
    /// Enter short / exit long
    Short,
    /// Reverse
    Reverse,
}

/// The states a `damper` robot can be in
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum State {
    /// Betting the oscillation will go up
    Long = 1,
    /// No bet
    Neutral = 0,
    /// Betting the oscillation will go down
    Short = -1,
}

impl State {
    /// Get the result of applying an action to a state
    pub fn act(self, action: Action) -> State {
        match self {
            State::Long => match action {
                Action::Long => State::Long,
                Action::Short => State::Neutral,
                Action::Reverse => State::Short,
            },
            State::Short => match action {
                Action::Long => State::Neutral,
                Action::Short => State::Short,
                Action::Reverse => State::Long,
            },
            State::Neutral => match action {
                Action::Long => State::Long,
                Action::Short => State::Short,
                Action::Reverse => State::Neutral,
            },
        }
    }
    /// Get the kinetic energy for a transition between two states
    pub fn ke(self, prev_state: State, energy: f64, bleed: f64) -> f64 {
        let state_int = self as i32 as f64;
        let prev_state_int = prev_state as i32 as f64;
        let delta = prev_state_int - state_int;
        energy * delta - bleed * delta.abs()
    }
    /// Get the potential energy of a state
    pub fn pe(self, energy: f64) -> f64 {
        let state_int = self as i32 as f64;
        state_int * energy
    }
    /// Apply an action to a state
    pub fn apply_act(&mut self, action: Action) {
        *self = self.act(action)
    }
}

/// A damping strategy
pub trait Strategy<Information> {
    /// Act on a piece of information
    fn act(
        &mut self,
        now: usize,
        energy: f64,
        prev_energy: f64,
        kinetic_energy: f64,
        potential_energy: f64,
        info: Information,
        curr_state: State,
        prev_action: Option<Action>,
        bleed: f64,
        failed: usize,
    ) -> Option<Action>;
}

/// The simplest possible damping strategy: bet with the current motion with given thresholds
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Impulse {
    /// Threshold to enter a long position
    pub long_threshold: f64,
    /// Threshold to enter a short position
    pub short_threshold: f64,
}

impl<I> Strategy<I> for Impulse {
    fn act(
        &mut self,
        _now: usize,
        energy: f64,
        prev_energy: f64,
        _kinetic_energy: f64,
        _potential_energy: f64,
        _info: I,
        _curr_state: State,
        _prev_action: Option<Action>,
        _bleed: f64,
        _failed: usize,
    ) -> Option<Action> {
        let flow = energy - prev_energy;
        if flow < self.short_threshold {
            Some(Action::Short)
        } else if flow > self.long_threshold {
            Some(Action::Long)
        } else {
            None
        }
    }
}

/// A driver for a damping strategy
pub trait Driver<Information> {
    /// Get the next piece of information and energy, if any
    fn info(&mut self, now: usize) -> Option<(Information, f64)>;
}

/// A linear driver, which changes a given amount each tick
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Linear {
    pub base: f64,
    pub slope: f64,
}

impl Linear {
    pub fn flatline(base: f64) -> Linear {
        Linear { base, slope: 0.0 }
    }
}

impl Driver<()> for Linear {
    fn info(&mut self, now: usize) -> Option<((), f64)> {
        Some(((), now as f64 * self.slope + self.base))
    }
}

/// Add random noise to a driver
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Noise<D, R> {
    pub driver: D,
    pub amplitude: f64,
    pub rng: R,
}

impl<D, I, R> Driver<I> for Noise<D, R>
where
    D: Driver<I>,
    R: Rng,
{
    fn info(&mut self, now: usize) -> Option<(I, f64)> {
        self.driver.info(now).map(|(info, signal)| {
            (
                info,
                signal + self.amplitude * self.rng.gen_range(-1.0, 1.0),
            )
        })
    }
}

/// Add random perturbations to a driver
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Perturb<D, R> {
    pub driver: D,
    pub amplitude: f64,
    pub perturbation: f64,
    pub rng: R,
}

impl<D, I, R> Driver<I> for Perturb<D, R>
where
    D: Driver<I>,
    R: Rng,
{
    fn info(&mut self, now: usize) -> Option<(I, f64)> {
        self.perturbation += self.amplitude * self.rng.gen_range(-1.0, 1.0);
        self.driver
            .info(now)
            .map(|(info, signal)| (info, signal + self.perturbation))
    }
}

/// Replay a driver's ticks
#[derive(Debug, Clone, PartialEq)]
pub struct Replay<I> {
    now: usize,
    record: Vec<Option<(I, f64)>>,
}

impl<I> Replay<I> {
    pub fn record<D: Driver<I>>(driver: &mut D, now: usize, ticks: usize) -> Replay<I> {
        let mut record = Vec::with_capacity(ticks);
        for tick in 0..ticks {
            record.push(driver.info(now + tick))
        }
        Replay { record, now }
    }
}

impl<I> Driver<I> for Replay<I>
where
    I: Clone,
{
    fn info(&mut self, now: usize) -> Option<(I, f64)> {
        if now < self.now {
            return None;
        }
        let ix = now - self.now;
        self.record.get(ix).cloned().flatten()
    }
}

/// An evaluation of a damping strategy
#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Evaluation {
    /// The number of transactions made
    pub transactions: usize,
    /// The number of actions completed
    pub actions: usize,
    /// The number of ticks traded
    pub ticks: usize,
    /// The maximum energy encountered
    pub max_energy: f64,
    /// The minimum energy encountered
    pub min_energy: f64,
    /// The final kinetic energy
    pub kinetic: f64,
    /// The final potential energy
    pub potential: f64,
}

impl Evaluation {
    /// The current energy level
    pub fn energy(&self) -> f64 {
        self.kinetic + self.potential
    }
    /// Evaluate the energy impact of an action
    pub fn evaluate_action(
        &mut self,
        state: State,
        prev_state: State,
        energy: f64,
        params: &Params,
    ) {
        self.potential = state.pe(energy);
        if state != prev_state {
            self.transactions += 1;
            self.kinetic += state.ke(prev_state, energy, params.bleed);
        }
        self.max_energy = self.max_energy.max(self.energy());
        self.min_energy = self.min_energy.min(self.energy());
        self.actions += 1;
    }
}

/// The evaluation context of a damping strategy
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct EvalCtx {
    /// The evaluation of the strategy
    pub eval: Evaluation,
    /// The previous action
    pub prev_action: Option<Action>,
    /// The current state
    pub curr_state: State,
    /// The current failure count
    pub failed: usize,
    /// The previous energy
    pub prev_energy: f64,
    /// The start time
    pub start_time: usize,
}

impl EvalCtx {
    pub fn new(
        start_time: usize,
        starting_state: State,
        starting_action: Option<Action>,
    ) -> EvalCtx {
        EvalCtx {
            eval: Evaluation::default(),
            prev_action: starting_action,
            curr_state: starting_state,
            prev_energy: f64::NAN,
            start_time,
            failed: 0,
        }
    }
    /// Get the number of ticks in this evaluation
    pub fn ticks(&self) -> usize {
        self.eval.ticks
    }
    /// Perform a tick
    pub fn tick<D, S, I>(&mut self, driver: &mut D, strategy: &mut S, params: &Params)
    where
        D: Driver<I>,
        S: Strategy<I>,
    {
        if let Some((info, energy)) = driver.info(self.ticks()) {
            if self.prev_energy.is_nan() {
                self.prev_energy = energy
            }
            if let Some(action) = strategy.act(
                self.ticks(),
                energy,
                self.prev_energy,
                self.eval.kinetic,
                self.eval.potential,
                info,
                self.curr_state,
                self.prev_action,
                params.bleed,
                self.failed,
            ) {
                let state = self.curr_state.act(action);
                self.eval
                    .evaluate_action(state, self.curr_state, energy, params);
                self.curr_state = state;
                self.prev_energy = energy;
                self.prev_action = Some(action);
            } else {
                self.prev_action = None;
            }
            self.failed = 0;
        } else {
            self.failed += 1;
        }
        self.eval.ticks += 1;
    }
}

/// The evaluation parameters of a damping strategy
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Params {
    pub bleed: f64,
}

/// Evaluate a damping strategy
pub fn evaluate_strategy<I, S, D>(
    strategy: &mut S,
    driver: &mut D,
    start_tick: usize,
    ticks: usize,
    starting_state: State,
    starting_action: Option<Action>,
    params: &Params,
) -> EvalCtx
where
    S: Strategy<I>,
    D: Driver<I>,
{
    let mut eval_ctx = EvalCtx::new(start_tick, starting_state, starting_action);
    for _ in 0..ticks {
        eval_ctx.tick(driver, strategy, params);
    }
    eval_ctx
}

#[cfg(test)]
mod tests {
    use super::*;
    use approx::assert_ulps_eq;

    #[test]
    fn impulse_strategy_leaves_flatline_driver_alone() {
        let mut flatline = Linear::flatline(582.0);
        let mut impulse = Impulse {
            long_threshold: 0.2,
            short_threshold: -0.2,
        };
        let impulse_eval = evaluate_strategy(
            &mut impulse,
            &mut flatline,
            0,
            100,
            State::Neutral,
            None,
            &Params { bleed: 1.0 },
        );
        assert_eq!(impulse_eval.eval.kinetic, 0.0);
        assert_eq!(impulse_eval.eval.potential, 0.0);
        let mut super_impulse = Impulse {
            long_threshold: 0.0,
            short_threshold: 0.0,
        };
        let super_impulse_eval = evaluate_strategy(
            &mut super_impulse,
            &mut flatline,
            0,
            100,
            State::Neutral,
            None,
            &Params { bleed: 1.0 },
        );
        assert_eq!(super_impulse_eval.eval.kinetic, 0.0);
        assert_eq!(super_impulse_eval.eval.potential, 0.0);
    }

    #[test]
    fn impulse_strategy_buys_rising() {
        let mut rising = Linear {
            base: 124.0,
            slope: 0.1,
        };
        let mut impulse = Impulse {
            long_threshold: 0.05,
            short_threshold: -0.05,
        };
        let impulse_eval = evaluate_strategy(
            &mut impulse,
            &mut rising,
            0,
            100,
            State::Neutral,
            None,
            &Params { bleed: 1.0 },
        );
        assert_eq!(impulse_eval.eval.kinetic, -125.1);
        assert_eq!(impulse_eval.eval.potential, 133.9);
        assert_ulps_eq!(impulse_eval.eval.energy(), 8.8, max_ulps = 10)
    }
}
