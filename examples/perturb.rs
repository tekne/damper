use damper::*;
use rand::thread_rng;

const ITERATIONS: usize = 100;
const BLEED: f64 = 0.1;
const PARAMS: Params = Params { bleed: BLEED };

fn main() {
    let mut impulse = Impulse {
        long_threshold: 0.1,
        short_threshold: 0.1,
    };
    let driver = Linear {
        base: 56.0,
        slope: 0.1,
    };
    let mut perturb = Perturb {
        driver,
        amplitude: 0.5,
        perturbation: 0.0,
        rng: thread_rng(),
    };
    println!("Iterations = {}, bleed = {}", ITERATIONS, BLEED);
    println!("Strategy pre-state = {:#?}", impulse);
    println!("Generator pre-state = {:#?}", perturb);
    let eval_ctx = evaluate_strategy(
        &mut impulse,
        &mut perturb,
        0,
        ITERATIONS,
        State::Neutral,
        None,
        &PARAMS,
    );
    println!("Strategy post-state = {:#?}", impulse);
    println!("Generator post-state = {:#?}", perturb);
    println!("Strategy evaluation = {:#?}", eval_ctx);
    println!("P&L = {}", eval_ctx.eval.energy());
}
